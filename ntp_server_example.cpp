// Distributed under the Boost Software License, Version 1.0.

/**
 * @file ntp_server_example.cpp
 * @author sb.kang@outlook.com
 * @date 03 March 2021
 */

#include "include/cpp_ntp/ntp_server.h"
#include <iostream>

int main() {
  std::cout << "ntp server working with port 1234\r\n";  
  ::ntp::ntp_server_t server(1234);
  server.run();

  return 0;
}