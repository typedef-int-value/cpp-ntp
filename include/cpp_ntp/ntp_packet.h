// Distributed under the Boost Software License, Version 1.0.

/**
 * @file ntp_packet.h
 * @author sb.kang@outlook.com
 * @date 03 March 2021
 */

#include <array>

namespace ntp {
/** make client packet. */
std::array<char, 48> make_request();

/** make server packet using client data. */
std::array<char, 48> make_response(char *data, size_t len);

/** get current time from server response. */
time_t time_from_respond(char *data, size_t len);

}  // namespace ntp
