// Distributed under the Boost Software License, Version 1.0.

/**
 * @file ntp_server.h
 * @author sb.kang@outlook.com
 * @date 03 March 2021
 */

#pragma once
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>
namespace ntp {

class ntp_server_t {
 public:
  ntp_server_t(unsigned short port = 123);
  ~ntp_server_t();

  void run();

 private:
  void start_receive();

  void handle_receive(const boost::system::error_code& error,
                      std::size_t bytes_transferred);

  void handle_send(std::shared_ptr<std::vector<char>> shared,
                   const boost::system::error_code& error,
                   std::size_t bytes_transferred);

  boost::asio::io_context io_context_;
  boost::asio::ip::udp::socket socket_;
  boost::asio::ip::udp::endpoint remote_endpoint_;
  boost::array<char, 65535> recv_buffer_;
};
}  // namespace ntp
