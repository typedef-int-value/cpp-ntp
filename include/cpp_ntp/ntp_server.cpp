// Distributed under the Boost Software License, Version 1.0.

/**
 * @file ntp_server.cpp
 * @author sb.kang@outlook.com
 * @date 03 March 2021
 */

#include "ntp_server.h"

#include "ntp_packet.h"

using boost::asio::ip::udp;

namespace ntp {

ntp_server_t::ntp_server_t(unsigned short port)
    : socket_(io_context_, udp::endpoint(udp::v6(), port)) {
  start_receive();
}

ntp_server_t::~ntp_server_t() {}

void ntp_server_t::run() { io_context_.run(); }

void ntp_server_t::start_receive() {
  socket_.async_receive_from(
      boost::asio::buffer(recv_buffer_), remote_endpoint_,
      boost::bind(&ntp_server_t::handle_receive, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));
}

void ntp_server_t::handle_receive(const boost::system::error_code& error,
                                  std::size_t bytes_transferred) {
  if (!error) {
    std::vector<char> data(recv_buffer_.begin(),
                           recv_buffer_.begin() + bytes_transferred);

    auto resp = ntp::make_response(data.data(), data.size());
    std::shared_ptr<std::vector<char>> shared(
        new std::vector<char>(resp.begin(), resp.end()));

    socket_.async_send_to(
        boost::asio::buffer(*shared), remote_endpoint_,
        boost::bind(&ntp_server_t::handle_send, this, shared,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));

    start_receive();
  }
}

void ntp_server_t::handle_send(std::shared_ptr<std::vector<char>> shared,
                               const boost::system::error_code& error,
                               std::size_t bytes_transferred) {}

}  // namespace ntp
