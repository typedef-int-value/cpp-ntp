// Distributed under the Boost Software License, Version 1.0.

/**
 * @file ntp_packet.cpp
 * @author sb.kang@outlook.com
 * @date 03 March 2021
 */

#include "ntp_packet.h"

#include <stdint.h>
#include <time.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/endian/conversion.hpp>

namespace ntp {

std::array<uint32_t, 2> ptime_to_timestamp(
    const boost::posix_time::ptime &time) {
  std::array<uint32_t, 2> ts = {0};
  auto from_1900 =
      time - boost::posix_time::ptime(boost::gregorian::date(1900, 1, 1));
  ts[0] = from_1900.ticks() /
          boost::posix_time::time_duration::rep_type::res_adjust();

  auto usec = from_1900.fractional_seconds();
  ts[1] = (4294 * (usec)) + ((1981 * usec) >> 11);

  return ts;
}

void gettime64(uint32_t *ts) {
  boost::posix_time::ptime current_date =
      boost::posix_time::microsec_clock::universal_time();

  auto timestamp = ptime_to_timestamp(current_date);

  ts[0] = timestamp[0];
  ts[1] = timestamp[1];
}
/* get Timestamp for NTP in LOCAL ENDIAN */
std::array<uint32_t, 2> gettime64() {
  std::array<uint32_t, 2> ts;
  gettime64(ts.data());
  return ts;
}

std::array<char, 48> make_request() {
  std::array<char, 48> buf = {0};
  /* LI VN MODE = 00 100 011*/
  buf[0] = 0x23;

  /* Transmit Timestamp */
  auto tts = gettime64();

  (*(uint32_t *)&buf[40]) = boost::endian::native_to_big(tts[0]);
  (*(uint32_t *)&buf[44]) = boost::endian::native_to_big(tts[1]);

  return buf;
}

std::array<char, 48> make_response(char *data, size_t len) {
  auto recv_time = gettime64();

  std::vector<char> recv_buf(data, data + len);
  std::array<char, 48> send_buf;

  /* do not use 0xC7 because the LI can be `unsynchronized` */
  if ((recv_buf[0] & 0x07 /*0xC7*/) != 0x3) {
    /* LI VN Mode stimmt nicht */
    throw std::logic_error("invalid packet");
  }

  /* LI/VN/Mode */
  send_buf[0] = (recv_buf[0] & 0x38) + 4;

  /* Stratum */
  send_buf[1] = 0x01;

  /* Reference ID = "LOCL" */
  *(uint32_t *)&send_buf[12] = boost::endian::native_to_big(0x4C4F434C);

  /* Copy Poll */
  send_buf[2] = recv_buf[2];

  /* Precision in Microsecond ( from API gettimeofday() ) */
  send_buf[3] = (signed char)(-6); /* 2^(-6) sec */

  uint32_t *u32p = (uint32_t *)&send_buf[4];

  /* Root Delay = 0, Root Dispersion = 0 */
  *u32p++ = 0;
  *u32p++ = 0;

  /* Reference ID */
  u32p++;

  gettime64(u32p);
  *u32p = boost::endian::native_to_big(*u32p - 60); /* -1 Min.*/
  u32p++;
  *u32p = boost::endian::native_to_big(*u32p); /* -1 Min.*/
  u32p++;

  /* Originate Time = Transmit Time @ Client */
  *u32p++ = *(uint32_t *)&recv_buf[40];
  *u32p++ = *(uint32_t *)&recv_buf[44];

  /* Receive Time @ Server */
  *u32p++ = boost::endian::native_to_big(recv_time[0]);
  *u32p++ = boost::endian::native_to_big(recv_time[1]);

  /* zum Schluss: Transmit Time*/
  gettime64(u32p);
  *u32p = boost::endian::native_to_big(*u32p); /* -1 Min.*/
  u32p++;
  *u32p = boost::endian::native_to_big(*u32p); /* -1 Min.*/

  return send_buf;
}

time_t time_from_respond(char *data, size_t len) {
  if (len < 48) throw std::logic_error("invalid packet");

  double tfrac = 4294967296.0;
  uint32_t client_tx_time[2] = {0}; /* t1 = Originate Timestamp  */
  uint32_t server_rx_time[2] = {0}; /* t2 = Receive Timestamp @ Server */
  uint32_t server_tx_time[2] = {0}; /* t3 = Transmit Timestamp @ Server */
  // uint32_t client_rx_time[2] = {0}; /* t4 = Receive Timestamp @ Client */

  auto client_rx_time = gettime64();
  uint32_t *pt = (uint32_t *)&data[24];

  client_tx_time[0] = boost::endian::big_to_native(*pt++);
  client_tx_time[1] = boost::endian::big_to_native(*pt++);

  server_rx_time[0] = boost::endian::big_to_native(*pt++);
  server_rx_time[1] = boost::endian::big_to_native(*pt++);

  server_tx_time[0] = boost::endian::big_to_native(*pt++);
  server_tx_time[1] = boost::endian::big_to_native(*pt++);

  double T1 = client_tx_time[0] + client_tx_time[1] / tfrac;
  double T2 = server_rx_time[0] + server_rx_time[1] / tfrac;
  double T3 = server_tx_time[0] + server_tx_time[1] / tfrac;
  double T4 = client_rx_time[0] + client_rx_time[1] / tfrac;

  time_t diff_sec = ((int32_t)(server_rx_time[0] - client_tx_time[0]) +
                     (int32_t)(server_tx_time[0] - client_rx_time[0])) /
                    2;

  time_t curr_time = time(NULL) + diff_sec;

  return curr_time;
}

}  // namespace ntp
